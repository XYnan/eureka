Eureka
=====

#### eureka-server

eureka-server 是项目的入口，主要包含了全局配置和子项目，核心内容是 webapp 下的 web.xml

web.xml 包含了一个 listener 和四个重要的 filter

listner：
- EurekaBootStrap 初始化行为

filter：
- statusFilter 状态相关功能
- requestAuthFilter 请求认证鉴权功能
- rateLimitingFilter 限流功能（手动开启
- gzipEncodingEnforcingFilter 压缩、编码功能

web.xml -> listner -> 4 个 filter -> jersy filter -> filter mapping -> welcome file